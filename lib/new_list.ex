defmodule NewList do
  def create() do
    input_list_name =
      IO.gets("Enter Your New List Name, Type exit to go back: ")
      |> String.trim()
      |> String.to_atom()

    case input_list_name do
      :exit ->
        TodoList.options()

      _ ->
        master_database(input_list_name)
        creating_list(input_list_name)
    end
  end

  def master_database(input_list_name) do
    master = :todo_master_table

    try do
      :ets.new(master, [:named_table, :bag])
    rescue
      ArgumentError -> master
    end

    :ets.insert(master, {input_list_name})
  end

  def creating_list(input_list_name) do
    try do
      :ets.new(input_list_name, [:named_table, :bag])
    rescue
      ArgumentError -> duplication_msg()
    end

    input_list_name
    |> IO.inspect(label: :input)

    IO.puts("Your Todo List has been Created !!!")
    IO.puts("-----------------------------------")

    TodoList.options()
  end

  defp duplication_msg() do
    IO.puts("This list already Exists, Kindly Enter another Name")
    create()
  end
end
