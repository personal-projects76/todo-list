defmodule TodoList do
  def options do
    IO.puts("Choose an option of your choice")
    IO.puts("A - Create a new List")
    IO.puts("B - Delete a List")
    IO.puts("C - Modify a List")
    IO.puts("D - View all Lists")
    IO.puts("E - Exit ToDo List")

    input =
      IO.gets("What's on your mind ?: ")
      |> String.trim()

    case input do
      "A" ->
        NewList.create()

      "B" ->
        IO.puts("----------------------------")
        IO.puts("Delete the list that you want to")

        delete_todo_list()
        IO.puts("Your selected List has been deleted\n")
        main_menu()

      "C" ->
        Modify.modify()

      "D" ->
        IO.puts("----------------------------")
        IO.puts("Here are all your todo lists")

        view_todo_lists()
        main_menu()

      "E" ->
        IO.puts("Exit Todo list")

      _ ->
        IO.puts("Wrong Input, kindly input again")
        options()
    end
  end

  def main_menu() do
    input = IO.gets("\nFor Main main menu press y ? ") |> String.downcase() |> String.trim()

    case input do
      "y" ->
        options()

      _ ->
        IO.puts("\n Wrong Option, kindly try again")
        main_menu()
    end
  end

  def view_todo_lists() do
    :ets.tab2list(:todo_master_table)
    |> Enum.map(fn {item} -> item end)
    |> Enum.each(fn item -> IO.puts(item) end)
  end

  def delete_todo_list() do
    view_todo_lists()
    input = IO.gets("\nWhich list do you want to delete ? ") |> String.trim() |> String.to_atom()
    :ets.delete_object(:todo_master_table, {input})
  end
end
